<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departament extends Model
{
    protected $fillable = [
        'name', 'color'
    ];

    public function users(){
        return $this->hasMany('App\User');
    }

    public function events(){
        return $this->hasMany('App\Event');
    }
}
