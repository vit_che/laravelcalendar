<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');
    }

    public function userstatus(Request $request)
    {
        $user = Auth::user();
        $role_user = User::find($user->id)->role->name;
        $user_name = Auth::user()->name;
        $dep_name_user = User::find($user->id)->departament->name;
        $user_status = false;

        if ($user) {
            $user_status = true;
        }


            return response()->json([
                'role' => $role_user,
                'name' => $user_name,
                'user_status' => $user_status,
            ],
                200);

    }

}
