<?php

namespace App\Http\Controllers\Calendar;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use DB;
use App\Departament;
use App\Role;
use Validator;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Validation\Rule;


class UserController extends Controller
{
    use SendsPasswordResetEmails;

    public function userstatus(Request $request)
    {
        $user = Auth::user();

        if (!$user) {

            return response()->json([
            ],
                401);
        }

        $role_user = User::find($user->id)->role->id;
        $user_name = Auth::user()->name;
        $user_status = false;

        if ($user) {
            $user_status = true;
        }

//        setcookie('remind', true);

        return response()->json([
            'role' => $role_user,
            'name' => $user_name,
            'status' => $user_status,
        ],
            200);
    }


    public function adduser(Request $request){

        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;

        if ($user_role_id == User::ROLE_ADMIN) {

            if ($request->isMethod('post')) {

                $input = $request->except('_token');

                $messages = [
                    'required' => 'Поле :attribute обязательно к заполнению',
                    'unique' => 'Поле :attribute уже занято',
                    'max' => "Поле:attribute  превышает 150 символов",
                    'min' => "Поле:attribute меньше 6 символов",
                ];

                $validator = Validator::make($input, [
                    'name' => 'required|max:150|unique:users',
                    'email' => 'required|email|max:150|unique:users',
                    'password' => 'required|min:6|max:150'
                ], $messages);

                if ($validator->fails()) {

                    return $this->validatorFails($validator);

                }

                $user = new User();

                $input['password'] = bcrypt($input['password']);

                $user->fill($input);

                if ($user->create($input)) {

                    $this->sendResetLinkEmail($request);

                    $successmsg = 'Пользователь Создан';

                    return $successmsg;
                }
            }

            $message = 'Uncorrect request';

            return $message;

        } else {

            $errormessage = 'У Вас нет прав для добавления Пользователя';

            return $errormessage;
        }
    }


    public function usersdata(){

        $users = User::all();
        $departaments = Departament::all();
        $roles = Role::all();

        $data = [
            'users' => $users,
            'departaments' => $departaments,
            'roles' => $roles
        ];

        return $data;
    }


    public function usersalldata()
    {

        $users = DB::table('users')
            ->join('departaments', 'users.departament_id', '=', 'departaments.id')
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->select('users.name as name', 'email', 'departaments.name as departament', 'roles.name as role', 'users.id as id')
            ->get();

        return $users;
    }


    public function userAndEventsDelete(Request $request)
    {
        $id_userdel = $request->id;

        $user_del = User::find($id_userdel);


            if ($request->isMethod('DELETE')) {

                if($user_del->delete()) {

                    $message = "User and related Events were deleted";

                    return $message;

                } else {

                    $message = "User and related Events were not deleted";

                    return $message;
                }
            }
    }

    public function resetpasswemail(Request $request){

        $email = $request->email;

        $user = User::where('email', $email)->first();

        if(is_null($user)) {

            $message = 'Your email doesn`t registered';

            return $message;
        }

        Controller::sendConfirm($email);

        return $email;
    }


    public function getuser(Request $request){

        $id = $request->id;

        $user = DB::table('users')
            ->join('departaments', 'users.departament_id', '=', 'departaments.id')
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->where('users.id', '=', $id)
            ->select('users.name as name', 'email', 'departaments.name as departament', 'departaments.id as depid','roles.name as role', 'roles.id as roleid','users.id as id')
            ->get();

        $departaments = Departament::all();
        $roles = Role::all();

        $user_events = User::find($id)->events;

        $data = [
            'user' => $user,
            'deps' => $departaments,
            'roles' => $roles,
            'events' => $user_events
        ];

        return $data;
    }


    public function edit(Request $request)
    {
        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;
        $user_id = $request->id;
        $user_edit = User::find($user_id);

        if ($user_role_id != User::ROLE_ADMIN) {
            //сообщение о превышении Прав
            $msg = 'You don`t have a right to Edit User!';

            return $msg;

        } elseif ($user_role_id == User::ROLE_ADMIN) {

            if ($request->isMethod('DELETE')) {

                if(!isset($request->id)) {

                    $message = "Выберите Пользователя!";

                    return $message;
                }

            $user_events = User::find($user_id)->events;

            $events_title = []; // related events

            foreach ($user_events as $event_title) {
                $events_title[] = $event_title->title;
            }

            if (count($events_title) > 0) {

                $user_has_events = "User is a perfomer of Events!
                                 Do you want delete related events?";

                $data = [
                    "events" => true,
                    "message" => $user_has_events,
                    "events_title" => $events_title
                ];

                return $data;
            }

                if ($user_edit->delete()) {

                    $msg = 'Пользователь Удален';

                    return $msg;

                } else {

                    $msg = 'Пользователь НЕ был Удален!!!';

                    return $msg;
                }

            } elseif ($request->isMethod('POST')) {

                if(!isset($request->id)) {

                    $message = "Выберите Пользователя!";

                    return $message;
                }

                $input = $request->except('_token');

                $messages = [
                    'required' => 'Поле :attribute обязательно к заполнению',
                    'unique' => 'Поле :attribute уже занято',
                    'max' => "Поле:attribute  превышает 150 символов",
                    'min' => "Поле:attribute меньше 6 символов"
                ];

                if($request->password == null  && User::find($user_id)->password != null){

                    $input['password'] = User::find($user_id)->password;
                }

                $validator = Validator::make($input, [
                    'name' => ['required', 'max:150', Rule::unique('users')->ignore($user_id)],
                    'email' => ['required', 'email', 'max:150', Rule::unique('users')->ignore($user_id)],
                    'password' => 'required|min:6|max:150'

                ], $messages);

                if ($validator->fails()) {

                    return $this->validatorFails($validator);
                }

                $input['password'] = bcrypt($input['password']);

                $user_edit->fill($input);

                if ($user_edit->update()) {

                    $msg = 'Изменения Сохранены';

                    return $msg;
                }
            }
        }
    }

    public function delUserChange(Request $request)
    {
        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;
        $user_id = $request->id;
        $user_edit = User::find($user_id);
        $user_events = User::find($user_id)->events;

        $admin_id = User::where('role_id', User::ROLE_ADMIN)->first()->id;

        if ($user_role_id != User::ROLE_ADMIN) {
            //сообщение о превышении Прав
            $msg = 'You don`t have a right to Edit User!';

            return $msg;

        } elseif ($user_role_id == User::ROLE_ADMIN) {

            $events_id = []; // related events id

            foreach ($user_events as $event_id) {
                $events_id[] = $event_id->id;
            }

            foreach ($events_id as $id) {

                Event::where('id', $id)
                    ->update(['user_id' => $admin_id]);
            }

            if ($user_edit->delete()) {

                $msg = 'Пользователь Удален c заменой исполнителя на Администратора';

                return $msg;

            } else {

                $msg = 'Пользователь НЕ был Удален!!!';

                return $msg;
            }
        }
    }
}
