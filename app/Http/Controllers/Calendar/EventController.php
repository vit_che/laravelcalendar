<?php

namespace App\Http\Controllers\Calendar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use function MongoDB\BSON\toJSON;
use Validator;
use Auth;
use App\Event;
use App\User;
use App\Departament;


class EventController extends Controller
{   /* get remind about events in the day  */
    public function getRemind(){

        $today = date('Y-m-d');

        $events = DB::table('events')
            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->select('title', 'start', 'end', 'departaments.name as departament', 'users.name as name', 'events.id as id')
            ->where('remind_date','<=', $today)
            ->where('events.start','>=', $today)
            ->orderBy('start')
            ->get();

        return $events;
    }

    /*  get Month Events List  */
    public function listEventsMonth(Request $request)
    {
        $start = $request->query('start');
        $end = $request->query('end');

        $events = DB::table('events')
            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
            ->select('title', 'events.start as start', 'events.end as end', 'departaments.name as name', 'departament_id', 'departaments.color as color')
            ->where('start','>=', $start)
            ->where('start','<=', $end)
            ->orderBy('start')
            ->get();
        /* get  Events array */
        $arr_event = [];

        $j = 0;

        foreach ($events as $ev){

            $days = $this->getEventTime($ev->start, $ev->end);

            if($days == 0){
                $days = 1;
            }

            $in_ev_start = $ev->start;

            for ($i = 0; $i < $days; $i++){

                $arr_item = $this->setArrItem($ev->name, $ev->departament_id, $in_ev_start, $ev->color, $ev->name);

                $arr_event[$j][$i] = $arr_item;

                $in_ev_start = $this->setNextDay($in_ev_start);
            }
            $j = $j + 1;
        }

        $arr_out = [];

        foreach($arr_event as $arr){
            $arr_out = array_merge($arr_out, $arr);
        }

        /*set title 1 */
        $new_arr = [];
        $e = 0;
        foreach ($arr_out as $out){
            $out['title'] = 1;
            $new_arr[$e] = $out;
            $e++;
        }
        /* group events by days & departaments */
        $evens_arr = [];

        foreach ( $new_arr as $ar ){

            if(  empty($evens_arr[$ar['start']][$ar['departament_id']] )  ) {

                $dep_name = $ar['name'];

                $evens_arr[$ar['start']][$ar['departament_id']] = $ar;
                $evens_arr[$ar['start']][$ar['departament_id']]['title'] .= ' '.$dep_name;

            } else {

                $dep_name = $ar['name'];

                $evens_arr[$ar['start']][$ar['departament_id']]['title'] += 1;
                $evens_arr[$ar['start']][$ar['departament_id']]['title'].=' '.$dep_name;
            }
        }
        /* merge array */
        $evens_out = [];

        foreach($evens_arr as $even){

            $evens_out = array_merge($evens_out, $even);
        }

        return response()->json($evens_out);
    }


    /* function set new array items */
    public function setArrItem($dep_name, $dep_id, $start, $color)
    {
        return  [ "title" => $dep_name, "departament_id" => $dep_id, "start" => $start, "color" =>$color, "name" => $dep_name ];
    }


    /* how many days has event  */
    public function getEventTime($start, $end){
        $t_start = strtotime($start);
        $t_end = strtotime($end);
        if (($start || $end) && ($end > $start)) {
            $days = idate('d', $t_end - $t_start);
        } else {

            return false;
        }

            return $days;
    }


    public function setNextDay($day){

        return date('Y-m-d', strtotime($day) + 86400);
    }


    public function listEventsDay(Request $request){

        $start = $request->query('start');
        $departament_id = $request->query('departament_id');

        $events_day = DB::table('events')
            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->select('events.id as id', 'title', 'start', 'end', 'users.name as perfomer', 'departaments.name')
            ->where('start','<=', $start)
            ->where('end', '>=', $start)
            ->where('events.departament_id','=', $departament_id)
            ->get();

        return response()->json($events_day);
    }


    public function getEvent(Request $request){
        $id = $request->id;
        $event = DB::table('events')
            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->select('title', 'description', 'start', 'end', 'users.name as perfomer','users.id as userid', 'departaments.name', 'departaments.id as depsid', 'remind_date')
            ->where('events.id', '=', $id)
            ->get();

        $departaments = Departament::all();
        $users = User::all();

        $data = [
            'event' => $event,
            'deps' => $departaments,
            'users' => $users
        ];
        return $data;
    }


    public function edit(Request $request){

        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;
        $event_id = $request->id;
        $event = Event::find($event_id);

        if ($user_role_id != User::ROLE_ADMIN) {
            //сообщение о превышении Прав
            $mistake = 'You don`t have a right to DELETE Event!';

            return $mistake;

        } elseif ( $user_role_id == User::ROLE_ADMIN ){

            if ($request->isMethod('DELETE')) {

                if($event->delete()) {

                    $msg = 'Событие Удалено';

                    return $msg;

                } else {

                    $msg = 'Событие НЕ было Удалено!!!';

                    return $msg;
                }

            } elseif ($request->isMethod('POST')){

                $input = $request->except('_token');

                $messages = [
                'required' => 'Поле :attribute обязательно к заполнению',
                'max' => "длина превышает 255 символов"
                ];

                $validator = Validator::make($input, [

                    'title' => 'required|max:255',

                ], $messages);

                if($validator->fails()){

                    return $this->validatorFails($validator);
                }

                $event->fill($input);

                if($event->update()) {

                    $msg = 'Изменения Сохранены';

                    return $msg;
                }
            }
        }
    }

    public function execute(Request $request)  // add Event
    {
        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;

        if ( $user_role_id == User::ROLE_ADMIN ) {

           return $this->_addEvent($request);

        } elseif ($user_role_id == User::ROLE_MODER) {

            return $this->_addEvent($request);

        } else {

            $message = "У Вас нет прав для этого действия!";

            return $message;
        }
    }

    public function _addEvent($request) {

        if ($request->isMethod('post')) {

            $input = $request->except('_token');

            $messages = [
                'required' => 'Поле :attribute обязательно к заполнению',
            ];

            $validator = Validator::make($input, [

                'title' => 'required|max:255',
                'start' => 'required'
            ], $messages);

            if ($validator->fails()) {

                return $this->validatorFails($validator);
            }

            $user = Auth::user();
            $event = new Event();

            $event->fill($input);
            $event->creator_id = $user->id;

            if ($event->end == null) {

                $event->end = $event->start;
            }

            if ($event->save()) {

                $message = "Событие Сохранено!";

                return $message;
            }
        }
    }

    public function getAllEvents(){

        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;

        if ( $user_role_id == User::ROLE_ADMIN) {

            return $this->getAllEventsIn();

        } elseif ( $user_role_id == User::ROLE_MODER ){

            return $this->getAllEventsIn();

        } else {

            $mistake = 'You don`t have a right!';

            return $mistake;
        }
    }

    private function getAllEventsIn(){

        $today = date('Y-m-d');
        $date = date('Y-m-d', strtotime($today) - 1296000);

        $events = DB::table('events')
            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->select('events.id as id','title', 'start', 'end', 'users.name as perfomer', 'departaments.name')
            ->where('start','>=', $date)
            ->where('end', '>=', $date)
            ->orderBy('start')
            ->get();

        return $events;
    }

    public function getCsvList(Request $request){

        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;
        $user_departament = User::find($user->id)->departament->id;

        if ( $user_role_id == User::ROLE_ADMIN ) {

            return $this->createAdminCsv($request);

        } elseif ($user_role_id == User::ROLE_MODER) {

            return $this->createModerCsv($request, $user_departament);

        } else {

            $message = "У Вас нет прав для этого действия!";

            return $message;
        }
    }

    private function createAdminCsv(Request $request){

        $start = $request->start;
        $end = $request->end;

        $events = DB::table('events')
            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->select('title', 'start', 'end', 'users.name as perfomer', 'departaments.name')
            ->where('start','>=', $start)
            ->where('start', '<=', $end)
            ->orderBy('start')
            ->get();

        $array = [];
        $i = 0;
        foreach($events as $event) {
            $ar = (array)$event;
            $array [$i] = $ar;
            $i++;
        }

        return $this->createLink($array);

    }

    private function createModerCsv(Request $request, $departament){

        $start = $request->start;
        $end = $request->end;

        $events = DB::table('events')
            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->select('title', 'start', 'end', 'users.name as perfomer', 'departaments.name')
            ->where('start','>=', $start)
            ->where('start', '<=', $end)
            ->where('departaments.id', '=', $departament)
            ->orderBy('start')
            ->get();

        $array = [];
        $i = 0;
        foreach($events as $event) {
            $ar = (array)$event;
            $array [$i] = $ar;
            $i++;
        }

        return $this->createLink($array);

    }

    private function createLink($array){

        $filename = '/home/dok/projects/websites/laravelcalendar/public/data.csv';

        $fp = fopen($filename, 'w');

        foreach ($array as $ev){
            fputcsv($fp, $ev);
        }

        fclose($fp);

        chmod($filename, 0700);

        header("content-type: application/csv");
        header('Content-Disposition: attachment; filename="data.csv"');

        $link = '/data.csv';

        return $link;
    }



    //////////////////////////////////
    ///
    ///
    public function file_force_download($file) {
        if (file_exists($file)) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
                ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            if ($fd = fopen($file, 'rb')) {
                while (!feof($fd)) {
                    print fread($fd, 1024);
                }
                fclose($fd);
            }
            exit;
        }
    }

    public function foo(){

        $filename = '/home/dok/projects/websites/laravelcalendar/public/data.csv';

        return $this->file_force_download($filename);

    }


}
