<?php

namespace App\Http\Controllers\Admin;

use App\Event_category;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

class AdminCategoryController extends Controller
{
    public function execute(){

        if (view()->exists('admin.categories.categoriesIndex')) {
            $categories = Event_category::all();
            $data = [
                'title' => 'Categories',
                'categories' => $categories
            ];

//            var_dump($events);
//            die();

            return view('admin.categories.categoriesIndex', $data);
        }
        abort(404);
    }

    public function addcategory(Request $request){

        if ($request->isMethod('post')) {

            $input = $request->except('_token');

            $messages = [
                'required' => 'Поле :attribute обязательно к заполнению',
                'unique' => 'Поле :attribute должно быть уникальным'
            ];

            $validator = Validator::make($input,[
                'name' => 'required|max:255|unique:event_categories',
//                'description' => 'required'
            ], $messages);

            if($validator->fails()) {
                return redirect()->route('categoryAdd')->withErrors($validator)->withInput();
            }

            $category = new Event_category();

            $category->fill($input);

            if($category->save()) {
                return redirect('admin')->with('status', 'Category was create');
            }
//            $user = Auth::user();
//            $article->author_name = $user->name;
//            Controller::ownerAdd($user, $article, 'article');
        }

        $category = Event_category::all();
//        $users = User::all();
        if(view()->exists('admin.categories.categoryAddIndex')) {
            $data = [
                'title' => 'New Category',
                'category' => $category,
//                'users' => $users
            ];
            return view('admin.categories.categoryAddIndex', $data);
        }
        abort(404);
    }


    public function editcategory(Event_category $category, Request $request)
    {
        //ONLY FOR ADMIN!!
        $user = Auth::user();

        if ($user->name == 'admin') {

            if ($request->isMethod('DELETE')) {
                $category->delete();
                return redirect('admin')->with('status', 'The Category was deleted');
            }
        } else {
            //сообщение о превышении Прав
            $mistake = 'You don`t have a right to DELETE Category!';
            return redirect('admin')->withErrors($mistake);
        }


        if($request->isMethod('POST')){
            $input = $request->except('_token');
            $validator = Validator::make($input, [
                'name' => 'required|max:255'
            ]);
            if($validator->fails()){
                return redirect()
                    ->route('categoryEdit', ['category'=>$input['id']])
                    ->withErrors($validator);
            }

            $category->fill($input);

            if($category->update()) {
                return redirect('admin')->with('status', 'Category was update');
            }
        }

        $old = $category->toArray();

        if(view()->exists('admin.categories.categoryEditIndex')){
            $data = [
                'title' => 'Editing category "'.$old['name'].'"',
                'data' => $old
            ];
            return view('admin.categories.categoryEditIndex', $data);
        }
    }
}
