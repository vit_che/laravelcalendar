<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Event;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use DB;

class AdminEventController extends Controller
{
    public function execute(){

        $user = Auth::user();
        $user_dep_name = User::find($user->id)->departament->name;
        $user_role_id = User::find($user->id)->role->id;


        if ($user_role_id == User::ROLE_ADMIN ) {

            if (view()->exists('admin.events.eventsIndex')) {

                $events = Event::orderBy('start')->get();

                $data = [
                    'title' => 'Events',
                    'events' => $events,
                ];

                return view('admin.events.eventsIndex', $data);
            }
            abort(404);

        } elseif ($user_role_id == User::ROLE_MODER ) {

            if (view()->exists('admin.events.eventsIndex')) {
                $events = Event::all()->where('departament', $user_dep_name);
                $data = [
                    'title' => 'Events',
                    'events' => $events
                ];

                return view('admin.events.eventsIndex', $data);
            }
            abort(404);
        } else {

//            TODO
            $data = [
                'title' => 'access denied'
            ];
//         return view('admin.events.eventAddIndex')->with('status', 'You don`t have access');
                                                                                    //            return view('layouts.admin', $data)->with('status', 'You don`t have access');
            return redirect('/admin')->withErrors('You don`t have access');
        }
    }


    public function relatedEvents(Request $request){



    }

//    public function responc(Request $request)
//    {
//        $start = $request->query('start');
//        $end = $request->query('end');
//
////        $events = DB::table('events')
////            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
////            ->select(DB::raw('departament_id, start, count(*) as title, departaments.color as color'))
////            ->where('start','>', $start)
////            ->where('start','<', $end)
////            ->groupBy('departament_id', 'start')
////            ->get();
//
////        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
////                $events = DB::table('events')
////                        ->join('departaments', 'events.departament_id', '=', 'departaments.id')
////                        ->select(DB::raw('departament_id, start, end, count(*) as title, departaments.color as color'))
////                        ->whereBetween('start', [$start, $end])
////                        ->groupBy('departament_id', 'start', 'end')
////                        ->get();
////
////                dump($events);
////
////                $events_arr = [];
////
////                foreach ( $events as $event){
////
////                    if(empty($events_arr[$event->start][$event->departament_id])) {
////
////                        $events_arr[$event->start][$event->departament_id] = $event;
////
////                    } else {
////
////                        $events_arr[$event->start][$event->departament_id]->title += $event->title;
////                    }
////
////                }
////
////                dump($events_arr);
////                die();
////
////        return response()->json($events_arr);
//
////        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//
//        $events = DB::table('events')
//            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
//            ->select(DB::raw('title, departament_id, start, end, departaments.color as color'))
//            ->where('start','>=', $start)
//            ->where('start','<=', $end)
//            ->get();
//
//        /* get  Events array */
//        $arr_event = [];
//
//        $j = 0;
//
//        foreach ($events as $event){
//
//            $days = $this->getEventTime($event->start, $event->end);
//
//            for ($i = 0; $i < $days; $i++){
//
//                $arr_item = $this->setArrItem($event->title, $event->departament_id, $event->start, $event->color);
//
//                $arr_event[$j][$i] = $arr_item;
//
//                $event->start = $this->setNextDay($event->start);
//            }
//            $j = $j + 1;
//        }
//
//        $arr_out = [];
//
//        foreach($arr_event as $arr){
//            $arr_out = array_merge($arr_out, $arr);
//        }
//
//        /*set title 1 */
//        $new_arr = [];
//        $e = 0;
//        foreach ($arr_out as $out){
//            $out['title'] = 1;
//            $new_arr[$e] = $out;
//            $e++;
//        }
//
//        /* group events by days & departaments */
//        $evens_arr = [];
//
//        foreach ( $new_arr as $ar ){
//
//            if(  empty($evens_arr[$ar['start']][$ar['departament_id']] )  ) {
//
//                $evens_arr[$ar['start']][$ar['departament_id']] = $ar;
//
//            } else {
//
//                $evens_arr[$ar['start']][$ar['departament_id']]['title'] += 1;
//            }
//        }
//
//        /* merge array */
//        $evens_out = [];
//
//        foreach($evens_arr as $even){
//
//            $evens_out = array_merge($evens_out, $even);
//        }
//
//        return response()->json($evens_out);
//
//    }
//
//    /* function set new array items */
//    public function setArrItem($title, $dep_id, $start, $color)
//    {
//        return  [ "title" => $title, "departament_id" => $dep_id, "start" => $start, "color" =>$color ];
//    }
//
//    public function getEventTime($start, $end){
//        $t_start = strtotime($start);
//        $t_end = strtotime($end);
//        if (($start || $end) && ($end > $start)) {
//            $days = idate('d', $t_end) - idate('d', $t_start);
//        } else {
//            return false;
//        }
//
//        return $days;
//    }
//
//    public function setNextDay($start){
//
//        return date('Y-m-d', strtotime($start) + 86400);
//    }
//
//    public function listEventsDay(Request $request){
//
//
//        $start = $request->query('start');
//        $end = $request->query('end');
//
//
//        if($end == null){
//            $end = $start;
//        }
//        $departament_id = $request->query('departament_id');
//
//        $events = DB::table('events')
//            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
//            ->select(DB::raw('title, start, end, departaments.name'))
//            ->where('start','<=', $start)
//            ->where('end', '>', $end)
//            ->where('departament_id','=', $departament_id)
//            ->get();
//
//
//        return response()->json($events);
//    }

//    public function getRemind(){
//
//        $today = date('Y-m-d');
//
//        $events = DB::table('events')
//            ->join('departaments', 'events.departament_id', '=', 'departaments.id')
//            ->select(DB::raw('title, start, end, departaments.name'))
//            ->where('remind_date','=', $today)
//            ->get();
//
//
//        return $events;
//    }

//    public function gotevent(Request $request){
//
//        $temp = $request->query('data');
//
//        if($temp){
//            session()->put('gotevent', $temp);
//        }
//
//        return $temp;
//
//    }


}
