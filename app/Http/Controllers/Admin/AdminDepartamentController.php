<?php

namespace App\Http\Controllers\Admin;

use App\Departament;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\User;

class AdminDepartamentController extends Controller
{
    public function execute(){

        if (view()->exists('admin.departaments.departamentsIndex')) {
            $departaments = Departament::all();
            $data = [
                'title' => 'Departaments',
                'departaments' => $departaments
            ];

//            var_dump($events);
//            die();

            return view('admin.departaments.departamentsIndex', $data);
        }
        abort(404);
    }

    public function adddepartament(Request $request){

        if ($request->isMethod('post')) {

            $input = $request->except('_token');

            $messages = [
                'required' => 'Поле :attribute обязательно к заполнению',
                'unique' => 'Поле :attribute должно быть уникальным'
            ];

            $validator = Validator::make($input,[
                'name' => 'required|max:255|unique:departaments',
//                'description' => 'required'
            ], $messages);

            if($validator->fails()) {
                return redirect()->route('departamentAdd')->withErrors($validator)->withInput();
            }

            $departament = new Departament();

            $departament->fill($input);

            if($departament->save()) {
                return redirect('admin')->with('status', 'Departament was create');
            }
//            $user = Auth::user();
//            $article->author_name = $user->name;
//            Controller::ownerAdd($user, $article, 'article');
        }

        $departament = Departament::all();
//        $users = User::all();
        if(view()->exists('admin.departaments.departamentAddIndex')) {
            $data = [
                'title' => 'New Departament',
                'departament' => $departament,
//                'users' => $users
            ];
            return view('admin.departaments.departamentAddIndex', $data);
        }
        abort(404);
    }


    public function editdepartament(Departament $departament, Request $request)
    {
        //ONLY FOR ADMIN!!
        $user = Auth::user();

        if ($user->name == 'admin') {

            if ($request->isMethod('DELETE')) {
                $departament->delete();
                return redirect('admin')->with('status', 'The Departament was deleted');
            }
        } else {
            //сообщение о превышении Прав
            $mistake = 'You don`t have a right to DELETE Departament!';
            return redirect('admin')->withErrors($mistake);
        }


        if($request->isMethod('POST')){
            $input = $request->except('_token');
            $validator = Validator::make($input, [
                'name' => 'required|max:255'
            ]);
            if($validator->fails()){
                return redirect()
                    ->route('departamentEdit', ['departament'=>$input['id']])
                    ->withErrors($validator);
            }

            $departament->fill($input);

            if($departament->update()) {
                return redirect('admin')->with('status', 'Departament was update');
            }
        }

        $old = $departament->toArray();

        if(view()->exists('admin.departaments.departamentEditIndex')){
            $data = [
                'title' => 'Editing departament "'.$old['name'].'"',
                'data' => $old
            ];
            return view('admin.departaments.departamentEditIndex', $data);
        }
    }

    public function depsdata(){

        $departaments = Departament::all();
        $users = User::all();

        $data = [
            'users' => $users,
            'deps' => $departaments
        ];


        return $data;

    }
}
