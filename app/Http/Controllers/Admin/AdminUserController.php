<?php

namespace App\Http\Controllers\Admin;

use App\Departament;
use App\Role;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Session;
//use Illuminate\Support\Facades\Mail;
//use App\Mail\MailClass;

class AdminUserController extends Controller
{
    public function execute(){ //show list Users

        if (view()->exists('admin.users.usersIndex')) {

//            $allusers = Role::find(3)->users;
//            $arr_us = [];
//            foreach ($allusers as $us){
//                $arr_us[] = $us->name;
//            }
//            dd($arr_us);

            $users = User::all();
            $data = [
                'title' => 'Users',
                'users' => $users
            ];

            return view('admin.users.usersIndex', $data);
        }
        abort(404);
    }

    public function adduser(Request $request){

        if ($request->isMethod('post')) {

            $input = $request->except('_token');

            $messages = [
                'required' => 'Поле :attribute обязательно к заполнению',
                'unique' => 'Поле :attribute должно быть уникальным',
                'max' => "длина превышает 255 символов"
            ];

            $validator = Validator::make($input,[
                'name' => 'required|max:255|unique:users',
                'email' => 'required|max:255|unique:users',
            ], $messages);

            if($validator->fails()) {

                return redirect()->route('userAdd')->withErrors($validator)->withInput();
            }

            $user = new User();

            $user->fill($input);

            $input['password'] = bcrypt($input['password']);

            if($user->create($input)) {

//                $email = $input['email'];   ССЫЛКА НА ВОСТАНОВЛЕНИЕ ПАРОЛЯ
//
//                Controller::sendConfirm($email);

                return redirect('admin')->with('status', 'User was create');
            }

        }

        $users = User::all();
        $departaments = Departament::all();
        $roles = Role::all();

        if(view()->exists('admin.users.userAddIndex')) {
            $data = [
                'title' => 'New User',
                'users' => $users,
                'departaments' => $departaments,
                'roles' => $roles
            ];
            return view('admin.users.userAddIndex', $data);
        }
        abort(404);
    }

    public function edituser(User $user, Request $request)
    {
        //ONLY FOR ADMIN!!
        $curuser = Auth::user();
        //check: does user have events?
        $user_has_events = User::find($user->id)->events;
        $user_id = $user->id;

        $user_events_title = [];

        foreach($user_has_events as $event){
            $user_events_title[] = $event->title;
        }

        if(count($user_events_title) > 0) {

//            return redirect('admin')->withErrors('The User has related events');
            return redirect('admin', $user_id)->withErrors('The User has related events');

        } else {

            if ($curuser->role->id == User::ROLE_ADMIN) {

                if ($request->isMethod('DELETE')) {
//                    $user->delete();
                    return redirect('admin')->with('status', 'The User was deleted');
                }
            } else {
                //сообщение о превышении Прав
                $mistake = 'You don`t have a right to DELETE User!';
                return redirect('admin')->withErrors($mistake);
            }

            if ($request->isMethod('POST')) {

                $input = $request->except('_token');
                $validator = Validator::make($input, [
                    'name' => 'required|max:255'
                ]);
                if ($validator->fails()) {
                    return redirect()
                        ->route('userEdit', ['user' => $input['id']])
                        ->withErrors($validator);
                }

                $user->fill($input);

                if ($user->update()) {
                    return redirect('admin')->with('status', 'User was update');
                }
            }

            $old = $user->toArray();

            $roles = Role::all();

            $old_departament = User::find($user->id)->departament->name;

            $old_role = User::find($user->id)->role->name;

            $departaments = Departament::all();

            if (view()->exists('admin.users.userEditIndex')) {
                $data = [
                    'title' => 'Editing user "' . $old['name'] . '"',
                    'data' => $old,
                    'departaments' => $departaments,
                    'old_departament' => $old_departament,
                    'old_role' => $old_role,
                    'roles' => $roles
                ];
                return view('admin.users.userEditIndex', $data);
            }
        }
    }

//    public function usersdata(){
//
//        $users = User::all();
//        $departaments = Departament::all();
//        $roles = Role::all();
//
//        $data = [
//            'users' => $users,
//            'departaments' => $departaments,
//            'roles' => $roles
//        ];
//
//        return $data;
//    }

}
