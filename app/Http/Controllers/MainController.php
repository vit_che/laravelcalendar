<?php

namespace App\Http\Controllers;

use App\Event;
use App\Us_category;
use Illuminate\Http\Request;
use App\User;
use DB;

class MainController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function showevents(){

        if (view()->exists('admin.events.eventsIndex')) {
            $events = Event::all();
            $data = [
                'title' => 'Events',
                'events' => $events
            ];

            return view('site.eventsListIndex', $data);
        }
        abort(404);
//        return view('site.index');
    }

    public function execute()
    {

        return view('site.index');
    }
}
