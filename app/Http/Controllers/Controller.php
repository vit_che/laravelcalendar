<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailClass;
use App\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /* added by me */
    public function getUserStatus(){

        $user = Auth::user();
        if ($user){

            return true;
        }
    }

    public function sendConfirm($email)
    {

        $confirm_code = hash('md5', $email);
//        $email = 'dokermast@gmail.com';    //test
        $message = new MailClass($confirm_code);

        Mail::to($email)->send($message);

        return $confirm_code;
    }



    public function confirmEmail($token)
    {
        $user = User::where('token', $token)
            ->first();

        if (isset($user)) {

            $user->verified = 1;

            if ($user->update()) {

                $status = 'You EMAIL IS VERIFIED SUCCESSFULLY!!';

                return redirect('home')->with('status', $status);

            } else {

                $error = 'Your Email verification data wasn`t saved! Try again.';

                return redirect('home')->withErrors( $error);
            }

        } else {

            $error = "You EMAIL IS NOT VERIFIED!!";

            return redirect('home')->withErrors( $error);
        }
    }

    public function validatorFails($validator) {

        $errors = $validator->errors();

        $arr_errors = [];
        $e = 0;
        foreach ($errors->all() as $err){
            $arr_errors[$e] = $err;
            $e++;
        }

        return $arr_errors;
    }

}
