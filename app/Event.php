<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $fillable = [
        'title', 'description', 'departament_id', 'creator_id','user_id','report', 'start', 'end', 'priority',
        'category', 'category_id', 'priority_id', 'remind_date'
    ];

    public function event_category() {
        return $this->belongsTo('App\Event_category');
    }

    public function departament() {
        return $this->belongsTo('App\Departament', 'departament_id');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function creator() {
        return $this->belongsTo('App\User', 'creator_id' );
    }


}
