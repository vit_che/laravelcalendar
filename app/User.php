<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable
{
    use Notifiable;
//    use CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    const ROLE_ADMIN = 1;
    const ROLE_MODER = 2;
    const ROLE_USER = 3;


    protected $fillable = [
        'name','role_id', 'role','departament_id', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function departament() {
        return $this->belongsTo('App\Departament');
    }

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function events() {
        return $this->hasMany('App\Event');
    }

    public function eventscreatman() {
        return $this->hasMany('App\Event');
    }

//    public function sendPasswordResetNotification($token)
//    {
//        $this->notify(new ResetPasswordNotification($token));
//    }

}
