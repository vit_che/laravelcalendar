<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailClass extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $code;

    protected $options = [];

    public function __construct($code)
    {
        $this->confirm_code = $code;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        $test = 'HELLO USER!';
        return $this->view('email.confirm_mail')
            ->with([
                'confirm_code' => $this->confirm_code,
            ]);
    }
}
