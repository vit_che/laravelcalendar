$('#info').dialog({
    autoOpen: false,
    width: 500,
    buttons: [
        {
            id: 'info',     
            text: 'Закрыть',
            click: function(){
                $('#info').dialog('close');
                $('#calendar').fullCalendar( 'refetchEvents');  
                // location.reload();              
            }
        }
    ],
    show: {
        effect: "drop",
        duration: 500
    },
    hide: {
        effect: "drop",
        duration: 500
    }
})
