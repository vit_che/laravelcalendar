
    /* LIST Users */
    $('#users_list').dialog({
        autoOpen: false,
        width: 900,
        show: {
            effect: "drop",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        }
    })

    /* B U T T O N    U S E R S    L I S T   S H O W  */

    $('#users_list_btn').click(function(){

        $('#users_list').dialog('open');

        $.ajax({
            type: "GET",
            url: "/users/usersalldata",
            data:{},            
            success: function(data){ 
                $('tr.del').remove();
                    
                data.forEach( function(el){
                $('#list_users').append('<tr id="tr'+el.id+'" class="del table-info edit_user" data-id="'+el.id+'"><td><a href="#">'
                + el.name + '</a></td><td>' + el.role + '</td><td>' + el.departament
                + '</td><td>' + el.email + '</td></tr>')
                });
                $('.edit_user').on('click', edit_user)
            },
            error: function(data){
                console.log('Users List Error!');
                $('#info').text(data);
                $('#info').dialog('open');
            }
        });
    });

    function edit_user(data){

        id = $(this).data('id');

        $.ajax({
            type: "GET",
            url: '/users/getuser',                                
            data: {
                id : id
            },       
            success: function(data){ 
                    
                $('#user_id').val(id);
                $('#user_name').val(data.user[0].name);
                $('#user_email').val(data.user[0].email);

                $('option.del').remove();

                $('#user_role').append('<option class="del" selected value='+data.user[0].roleid+'>' + data.user[0].role + '</option>');
                data.roles.forEach( function(el){
                    $('#user_role').append('<option class="del" value='+el.id+'>' + el.name + '</option>')
                }); 

                $('#user_departament').append('<option class="del" selected value='+data.user[0].depid+'>' + data.user[0].departament + '</option>');           
                data.deps.forEach( function(el){
                    $('#user_departament').append('<option class="del" value='+el.id+'>' + el.name + '</option>')
                }); 

                $( "#creat_user_form" ).dialog( {width: 500});
                $( "#creat_user_form" ).dialog('open');
                $('#users_list').dialog('close');
            },
            error: function(data){
                console.log(' Error!');
                $('#info').text(data);
                $('#info').dialog('open'); 
            }                                           
        });
    }

/*   D E L E T E     U S E R    M E T H O D   */ 

    $('#dialog_user_delete').dialog({
        autoOpen: false,
        buttons: [ 
            {   
                id: 'cancel_del_user',
                text: 'Отмена',
                click: function() { 
                    $('#dialog_user_delete').dialog('close');
                }
            },
            {
                id: 'del_user_change',
                text: 'Удалить',
                click: function() { 

                    id = $('#user_id').val();

                    $.ajax({                        
                        type: "POST",
                        url: "/users/delUserChange",
                        data: {
                            id:id
                        },
                        success: function(data){
                            console.log('UserChangeDeleteADmin Success');
                            $('#dialog_user_delete').dialog('close');
                            $('#info').text(data);
                            $('#info').dialog('open');
                        },
                        error: function(data){
                            console.log('Error');
                            $('#dialog_user_delete').dialog('close');
                            $('#info').text(data);
                            $('#info').dialog('open');
                        }
                    });
                }
            }
        ],
        show: {
            effect: "drop",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        }
    });

    /* call D I A L O G    W I N D O W  for confirm Delete */

    function del_btn(){
        $('#dialog_user_delete').dialog('open');

        var del_user = $(this).data("id"); 
        
        $('#delete_user').click(function(){
            $.ajax({
                type: "DELETE",
                url: "/users/userdelete",
                data: {
                    id: del_user
                },                
                success: function(data){
                    console.log(data);
                    $('#dialog_user_delete').dialog('close');
                    $('#users_list').dialog('close');        
                },
                error: function(data){
                    $('#info').text(data);
                    $('#info').dialog('open');
                }
            });                   
        });
        $('#cancel_delete_user').click(function(){
            $('#dialog_user_delete').dialog('close');
            return false;
        });
    };

/* e n d     D E L E T E     U S E R     */ 

    $('#user_creat_btn').click(function(){
        clear_user_form();
        $('#users_list').dialog('close');
        $.ajax({
            type: "GET",
            url: '/users/usersdata',
            
            success: function(data){
                $('option.del').remove();
                data.roles.forEach( function(el){
                    $('#user_role').append('<option class="del" value='+el.id+'>' + el.name + '</option>')
                }); 
                data.departaments.forEach( function(el){
                    $('#user_departament').append('<option class="del" value='+el.id+'>' + el.name + '</option>')
                });                   
            },
            error: function(data){
                console.log('Out with Error');             
                $('#info').text(data);
                $('#info').dialog('open');  
            },
            show: {
                effect: "drop",
                duration: 500
            },       
            hide: {
                effect: "drop",
                duration: 500
            }  
        });
        $( "#creat_user_form" ).dialog({ width: 500 });
        $('#creat_user_form').dialog('open');       
    });

    /* A D D / E D I T  /  D E L E T E    U S E R    F O R M  */
    $( "#creat_user_form" ).dialog({
        autoOpen: false,
        buttons: [ 
            {
                id: 'creat_user_btn',
                text: 'Создать юзера',
                click: function() { 

                    var user_name = $('#user_name');
                    var user_email = $('#user_email');
                    var user_role = $('#user_role');
                    var departament = $('#user_departament');
                    var password = $('#user_password');

                    if(user_name.val() == '') {
                        alert('Введите имя');
                        return false;
                    }
                    if(user_email.val() == '') {
                        alert('Введите email');
                        return false;
                    }
                    if(password.val() == '') {
                        alert("Введите пароль");
                        return false;
                    }
                                 
                    $.ajax({
                        type: "POST",
                        url: "/users/add",
                        
                        data: {
                            name: user_name.val(),
                            email: user_email.val(),
                            departament_id: departament.val(),
                            role_id: user_role.val(),
                            password: password.val() 
                        },
                        success: function(data){
                            console.log('success user add');                       
                            $('#creat_user_form').dialog('close');
                            clear_user_form();
                            $('#info').text(data);
                            $('#info').dialog('open');
                        },
                        error: function(data){
                            console.log('Error users add');                            
                            $('#info').text(data.responseText);
                            $('#info').dialog('open');
                        }
                    });
                }
            },
            {
                id: 'edit_user_btn',
                text: 'Изменить пользователя',
                click: function() { 

                    var user_id = $('#user_id').val();
                    var user_name = $('#user_name').val();
                    var user_email = $('#user_email').val();
                    var user_role = $('#user_role').val();
                    var departament = $('#user_departament').val();
                    var password = $('#user_password').val();
                                 
                    if(user_id == '') {
                        alert("Пользователь не выбран");
                        return false;
                    }
                    if(user_name == '') {
                        alert("Введите имя");
                        return false;
                    }
                    if(user_email == '') {
                        alert("Введите email");
                        return false;
                    }
                    if(user_password == '') {
                        alert("Введите пароль");
                        return false;
                    }
                    

                    $.ajax({
                        type: "POST",
                        url: "/users/edit",
                        
                        data: {
                            id: user_id,
                            name: user_name,
                            email: user_email,
                            departament_id: departament,
                            role_id: user_role,
                            password: password
                        },
                        success: function(data){
                            $('#creat_user_form').dialog('close');
                            clear_user_form();

                            $('#info').text(data);
                            $('#info').dialog('open');
                        },
                        error: function(data){
                            console.log('Error');
                            $('#info').text(data);
                            $('#info').dialog('open');
                        }
                    });
                }
            },
                        {
                id: 'del_user_btn',
                text: 'Удалить пользователя',
                click: function() { 

                    var user_id = $('#user_id').val();

                    if(user_id == '') {
                        alert("Пользователь не выбран");
                        return false;
                    }
       
                    $.ajax({
                        type: "DELETE",
                        url: "/users/edit",
                        
                        data: {
                            id: user_id,
                        },
                        success: function(data){
                            $('#creat_user_form').dialog('close');
                            if(data.events){
                                console.log("есть события");
                                $('#dialog_user_delete').dialog('open');
                            } else {
                                $('#info').text(data);
                                $('#info').dialog('open');
                            }
                        },
                        error: function(data){
                            console.log('Delete Error');
                            $('#info').text(data);
                            $('#info').dialog('open');
                        }
                    });
                }
            },
            {   
                id: 'dnt_cr_us',
                text: 'Отмена',
                click: function() { 
                    $('#creat_user_form').dialog('close');
                }
            }
        ],
        show: {
        effect: "drop",
        duration: 500
        },
        hide: {
        effect: "drop",
        duration: 500
        }
    });
    $('#close_user_list_btn').click(function(){
        $('#users_list').dialog('close');
    })
    /* END  Add Users Form*/
    function clear_user_form(){
        $('#user_name').val("");
        $('#user_email').val("");
        $('#user_role').val("");
        $('#user_departament').val("");
        $('#user_password').val("");
    };
