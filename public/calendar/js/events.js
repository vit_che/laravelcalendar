
/* modal window for    A D D / E D I T  / D E L E T E     E V E N T   */ 

$("#events" ).dialog({
    autoOpen: false,
    buttons: [
        {
            id: 'add',
            text: 'Добавить',
            click: function(data) {
                var event_start = $('#start');
                var event_end = $('#end');
                var event_remind = $('#remind');                   
                var event_title = $('#title');
                var event_description = $('#description');
                var event_departament = $('#departament');
                var event_perfomer = $('#perfomer');

                if(event_title.val() == '') {
                    alert('Введите название события');
                    return false
                }

                if (event_end.val() === ''){
                    event_end.val(event_start.val());
                }
                if(event_start.val() > event_end.val()) {
                    alert('Дата окончания не может быть раньше Даты начала');
                    return false;
                }
                if(event_remind.val() >= event_start.val()) {
                    alert('Дата напоминания не может быть позже или равна Дате начала');
                    return false;
                }
                
                $.ajax({
                    type: "POST",
                    url: "/events/add",
                    
                    data: {
                        title: event_title.val(),
                        start: event_start.val(),
                        end: event_end.val(),
                        description: event_description.val(), 
                        departament_id: event_departament.val(),
                        user_id: event_perfomer.val(),
                        remind_date: event_remind.val(),
                    },
                    response: 'text',
                    success: function(data){
                        console.log('success');
                        $('#events').dialog('close');

                        $('#info').text(data);
                        $('#info').dialog('open');

                        event_title.val("");
                        event_start.val("");
                        event_description.val(""); 
                        event_departament.val("");
                        event_end.val("");
                        event_perfomer.val("");
                        event_remind.val("");
                        $('#calendar').fullCalendar( 'refetchEvents');
                    },
                    error: function(data){
                        console.log(' Error!');

                        $('#info').text(data);
                        $('#info').dialog('open');
                    }   
                });
            }
        }, 
        {   
            id: 'edit_event',
            text: 'Изменить',
            click: function(data) {
                                    
                var event_id = $('#event_id');
                var event_start = $('#start');
                var event_end = $('#end');
                var event_remind = $('#remind');                   
                var event_title = $('#title');
                var event_description = $('#description');
                var event_departament = $('#departament');
                var event_perfomer = $('#perfomer');

                if(event_id.val() == '') {
                    alert('Выберите событие!');
                    return false;
                }
                if (event_end.val() === ''){
                    event_end.val(event_start.val());
                }
                if(event_start.val() > event_end.val()) {
                    alert('Дата окончания не может быть раньше Даты начала');
                    return false;
                }
                if(event_remind.val() >= event_start.val()) {
                    alert('Дата напоминания не может быть позже или равна Дате начала');
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: '/events/edit',
                    
                    data: {
                        id: event_id.val(),
                        title: event_title.val(),
                        start: event_start.val(),
                        end: event_end.val(),
                        description: event_description.val(), 
                        departament_id: event_departament.val(),
                        user_id: event_perfomer.val(),
                        remind_date: event_remind.val(),
                    },
                    response: 'text',
                    success: function(response){
                        $('#events').dialog('close');
                        $('#info').text(response);
                        $('#info').dialog('open');

                        event_id.val(""),
                        event_title.val("");
                        event_start.val("");
                        event_description.val(""); 
                        event_departament.val("");
                        event_end.val("");
                        event_perfomer.val("");
                        event_remind.val("");
                        $('#calendar').fullCalendar( 'refetchEvents');
                    },
                    error: function(data){
                        console.log(' Error!' + data);
                        $('#info').text(data);
                        $('#info').dialog('open');
                    }   
                });  
            }
        },
        {   
            id: 'cancel_event',
            text: 'Отмена',
            click: function() { 
                $(this).dialog('close');
            }
        },
        {
            id: 'delete_event',
            text: 'Удалить',
            click: function() { 

                var event_id = $('#event_id').val();
                console.log(' delete id = ' + event_id);

                if(event_id == '') {
                    alert('Выберите событие!');
                    return false;
                }

                $.ajax({
                    type: "DELETE",
                    url: "/events/edit",

                    data: {
                        id: event_id,
                    },
                    response: 'text',
                    success: function(response){
                        console.log(response);
                        $('#events').dialog('close');
                        $('#info').text(response);
                        $('#info').dialog('open');
                    },
                    error: function(data){
                        console.log(' Error!' + data);
                        $('#info').text(data);
                        $('#info').dialog('open');
                    } 
                });
            },
        }
    ],      
    show: {
        effect: "drop",
        duration: 500
    },
    hide: {
        effect: "drop",
        duration: 500
    }
});

 /* function gets ONE event DATA for EDITION */
 function show_event(data){

    console.log('ID = ' + $(this).data("id"));

    id = $(this).data("id");

    $('#event_view').dialog('close');              

     $.ajax({
         type: "GET",
         url: '/events/getEvent',                                
         data: {
             id : $(this).data("id")
         },       
         success: function(data){ 
             $( "#events" ).dialog({ width: 800 });       
             $('#event_id').val(id);
             $('#start').val(data.event[0].start);
             $('#end').val(data.event[0].end);
             $('#remind').val(data.event[0].remind_date);                   
             $('#title').val(data.event[0].title);               
             $('#description').val(data.event[0].description);
             $('#departament').text(data.event[0].name);
             $('#perfomer').text(data.event[0].perfomer);

             $('#departament').append('<option class="del" selected value='+data.event[0].depsid+'>' + data.event[0].name + '</option>');
             data.deps.forEach( function(el){
                 $('#departament').append('<option class="del" value='+el.id+'>' + el.name + '</option>')
             }); 

             $('#perfomer').append('<option class="del" selected value='+data.event[0].userid+'>' + data.event[0].perfomer + '</option>');           
             data.users.forEach( function(el){
                 $('#perfomer').append('<option class="del" value='+el.id+'>' + el.name + '</option>')
             }); 
             $( "#events" ).dialog('open');

         },
         error: function(data){
             console.log('Get Event Error!');
         }                                           
     });
 }

$( "#remind_events" ).dialog({
     autoOpen: false,
 });
 
  /* datapicker for adding event*/
 $('.datepicker').datepicker({dateFormat: "yy-mm-dd"});

 /*  EVENT INFO VIEW  */

 $('#event_view').dialog({
     autoOpen: false,
 });

/* Got Event Button */

 $('#got_event').click(function(){
     $('#remind_events').dialog('close');
 });

 $('#close_day_events').click(function () {
     $('#event_view').dialog('close');
 });

 /*  E V E N T S      L I S T   */

 $('#events_list_btn').click(function(){
     $('#events_list').dialog('open');
     $.ajax({
         type: "GET",
         url: '/events/getAllEvents',     
         success: function(data){ 
             $('tr.del').remove();        
             data.forEach( function(el){
                 $('#events_list_tr').append('<tr class="del table-info" data-id="'+ el.id + '"><td>'
                  + el.start + '</td><td>' + el.end + '</td><td class="show_event" data-id="'+ el.id + '">'+ el.title + '</td><td>' + el.perfomer +
                 '</td><td><button class="event_delete_btn ui-button ui-widget ui-corner-all" data-id="' + el.id + '">Delete</button></td></tr>')
             }); 
             $('.event_delete_btn').on('click', event_del);
             $('.show_event').on('click', show_event);  
         },
         error: function(data){
             console.log(' Events List Error!');
             $('#info').text(data);
             $('#info').dialog('open'); 
         }                                           
     });
 });
 
 $('#events_list').dialog({
     autoOpen: false,
     width: 800,
     buttons: [
         {   
             id: 'cancel_event',
             text: 'Отмена',
             click: function() { 
                 $(this).dialog('close');
             }
         }
     ],
     show: {
         effect: "drop",
         duration: 500
     },
     hide: {
         effect: "drop",
         duration: 500
     }
 });

 function event_del(){
     id = $(this).data('id');
     $.ajax({
             type: "DELETE",
             url: "/events/edit",
         data: {
             id: id
         },                
         success: function(data){
             $('#info').text(data);
             $('#info').dialog('open');
             $('#events_list').dialog('close');                
         },
         error: function(data){
             $('#info').text(data);
             $('#info').dialog('open');
         }
     });  
 }
