
$(document).ready(function() {

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
          "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };

    $.ajaxSetup({ headers: { 'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") } });

    // var user_role;
    // var user_name;

    $.ajax({
        type: "GET",
        url: '/users/status',
        success: function(data){
            console.log(data);
            $('#login_alt').hide();
            if(data.role == 1) {
                $('#admin_btn').show();
            }     
            if (data.role == 2) {
                $('#admin_btn').show();
            }

            $('#visit').hide();
            $('body').removeClass('back');
            $('body').addClass('backauth');
            $('#greeting').text('Hello, ' + data.name);
            // $('#user_info_name').text('Name: ' + data.name);
            // $('#user_info_status').text('Status: ' + data.status);
            // $('#user_info_role').text('Role id : ' + data.role);
            // $('#calendar').fullCalendar( 'refetchEvents');
            calendar();
            remind();   
        },
        error: function(data){
            console.log('get Status with Error');
            $('#logout_alt').hide();

            $('#main').hide();
            $('#calendar').hide();
            $('body').removeClass('backauth');
            $('body').addClass('back');
        }
    });

/*  REMIND EVENTS */
    function remind(){

        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") ,
            }
        });

        $.ajax({       
            type: "GET",
            url: '/events/getRemind',
            success: function(data){
                
                $('tr.del').remove();
                data.forEach( function(el){
                    $('#remindListEvents').append('<tr class="del table-warning show_event" data-id="'+ el.id +'"><td>' + 
                    el.start + '</td><td><a class="show_event" href="#">'+ el.title + '</a></td><td>' + el.departament + '</td><td>' + el.name + '</td></tr>')
                });
                if(data.length > 0) {
                    $( "#remind_events" ).dialog({
                        width: 1000,
                        height: 800
                    });
                    $('#remind_events').dialog('open'); 
                    $('.show_event').on('click', show_event);

                } else { 
                    $('#remind_events').dialog('close');  
                }    
            },
            error: function(data){
                console.log('No auth');               
            }
        });
    }
    /*   C A L E N D A R  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! C A L E N D A R  ! ! ! ! ! ! ! ! ! ! ! ! ! ! */
function calendar () {
    $('#calendar').fullCalendar({       
        themeSystem: 'jquery-ui',
        locale: 'ru',
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'LogoutButton, LoginButton'
        },
        events: '/events/listEventsMonth',     
        eventRender: function(event, element) {  //подключены доп js  css
            element.qtip({
                content: event.description
            });
        },
    /* E V E N T   C R E A T E  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! */
        dayClick: function(date, jsEvent) {  
            $.ajax({
                type: "GET",
                url: '/departaments/depsdata',
                            
                success: function(data){
                    console.log('DayClick Success');
                    if(data.error){
                        alert(data.error);
                        return false;
                    }
                    $('option.del').remove();
                    
                    data.deps.forEach( function(el){
                        $('#departament').append('<option class="del" value='+el.id+'>' + el.name + '</option>')
                    }); 
                    data.users.forEach( function(el){
                        $('#perfomer').append('<option class="del" value='+el.id+'>' + el.name + '</option>')
                    });  
                    var clickDate = date.format();
                    var title = $('#title').val();
                    $('#start').val(clickDate);
                    $( "#events" ).dialog({ width: 800 });
                    $( "#events" ).dialog('open');
                },
                error: function(data){
                    console.log('Error' + data);
                }     
            });
        },
    /* E V E N T   S H O W  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! */
        eventClick: function(events) {
            $.ajax({
                type: "GET",
                url: '/events/listEventsDay',                                
                data: {
                    start : events.start._i,
                    departament_id : events.departament_id
                },       
                success: function(data){ 
                    console.log('Event Click Success');
                    $('#event_title').text("Название отдела : " +data[0].name);

                    $('tr.del').remove();
                    
                    data.forEach( function(el){
                        $('#listEvents').append('<tr class="del table-info show_event" data-id="'+ el.id + '"><td>' + el.start + '</td><td>' + el.end + '</td><td>'
                        + el.title + '</td><td>' + el.perfomer + '</td>' +
                        '</tr>')
                    });   
                    $('.show_event').on('click', show_event);
                },
                error: function(data){
                    console.log(' Error!');
                    console.log(data);  
                }                                           
            });
            $( "#event_view" ).dialog({
              width: 800
            });
            $('#event_view').dialog('open');              
        },
    }); 
};

//   L O G I N    ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !    L O G I N    ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !

/* login form */
    $( "#login" ).dialog({

        autoOpen: false,
        buttons: [ 
            {
                id: 'enter',
                text: 'Войти',
                click: function() { 
                    var user_name = $('#name');
                    var user_password = $('#password');

                    $.ajaxSetup({
                        headers: {
                            'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") ,
                        }
                    });

                    $.ajax({                        
                        type: "POST",
                        url: "/login",
                        data: {
                            name: user_name.val(),
                            password: user_password.val(), 
                        },
                        success: function(data){
                            $('#login').dialog('close');
                            $('#calendar').fullCalendar( 'refetchEvents'  );
                            location.reload();
                        },
                        error: function(data){
                            $('#login').dialog('close');
                            $('#info').removeClass();
                            $('#info').addClass('ui-state-highlight');
                            if (data.responseJSON.name == undefined) {
                                $('#info').text("No connect with server!");
                            } else {
                                $('#info').text(data.responseJSON.name);
                            }                      
                            $('#info').dialog('open');
                        }
                    });
                }
            }, 
            {   
                id: 'dont',
                text: 'Отмена',
                click: function() { 
                    $('#login').dialog('close');
                }
            }
        ],
        show: {
        effect: "drop",
        duration: 500
        },
        hide: {
        effect: "drop",
        duration: 500
        }
    });
    $('#forgot').click(function () {
        $('#login').dialog('close');
        $('#forgot_form').dialog('open');
    });
    $('#forgot_send_link').click(function () {
        console.log("Click Reset Paswd");
        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") ,
            }
        });
        var email = $('#forgot_email');
        $.ajax({
            type: "POST",
            url: "/password/email",
            data: {
                email: email.val(),
            },
            success: function(data){
                console.log('Send Link Success');
                $('#forgot_form').dialog('close');
            },
            error: function(data){
                console.log('Send Link Error');
                $('#forgot_form').dialog('close');
                $('#info').text(data);
                $('#info').dialog('open');
            }
        });
        $('#forgot_form').dialog('close');
    }),
    $( "#forgot_form" ).dialog({
        autoOpen: false,
        show: {
            effect: "drop",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        }
    });
    $('#save_new_password').click(function () {
        console.log("Click Save New Paswd");
        $('#change_password_form').dialog('close');
    }),
    $( "#change_password_form" ).dialog({
        autoOpen: false,
        show: {
            effect: "drop",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        }
    });

/* End login form */

/* test Button Login */
    $('#login_btn').click(function() {
        
        $( "#login" ).dialog('open');
    });
    
    /* test Button Logout */
    $('#logout_alt').click(function() {
        $.ajax({
            type: "POST",
            url: "/logout",   
                
                success: function(){ 
                    console.log('You Escaped Successfully');
                    $('body').removeClass('backauth');
                    $('body').addClass('back');  
                    document.location.href = 'http://laravelcalendar.example/calendar/index.html';                                 

                },          
                error: function(data){
                    console.log('You Escaped with Error!');
                    console.log(data);
                    $('#info').text(data);
                    $('#info').dialog('open');
                }                  
        })
    });

// GET CSV LIST 
    $('#get_csv').click(function(){
        $('#dialog_download_events').dialog('open');
    })

    $('#dialog_download_events').dialog({
        autoOpen: false,
        buttons: [ 
            {   
                id: 'cancel_download',
                text: 'Отмена',
                click: function() { 
                    $('#dialog_download_events').dialog('close');
                }
            },
            {
                id: 'get_csv_list',
                text: 'Загрузить',
                click: function() { 
                    var start = $('#startList').val();
                    var end = $('#endList').val();

                    $.ajaxSetup({
                        headers: {
                            'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") ,
                        }
                    });

                    $.ajax({                        
                        type: "GET",
                        url: "/events/getCsvList",                        
                        data: {
                            start: start,
                            end: end,
                        },
                        success: function(data){
                            console.log(data); //for test
                            $('#dialog_download_events').dialog('close');
                            // $('a.del').remove();
                            $('#link').dialog('open');
                            $('#link').append('<button id="link_btn" class="del ui-button ui-widget ui-corner-all" ><a class="del " href='+ data + '>Загрузить</a></button>');
                            $('#link_btn').click(function(){
                                $('#link').dialog('close');
                                $('#link_btn').remove();
                            });                   
                        },
                        error: function(responce){
                            console.log('Error');
                        }
                    });
                }
            }
        ],
        show: {
            effect: "drop",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        }
    });

    $('#link').dialog({
        autoOpen: false,
    });

//  F U N C T I O N S !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    $(function () {
        $('#departament_color').colorpicker();
    });
});

