    /* D E P A R T A M E N T S   L I S T    S H O W     B U T T O N  */

$('#deps_list_btn').click(function(){
    $('#departaments_list').dialog('open');
    $.ajax({
        type: "GET",
        url: "/departaments/depsdata",

        success: function(data){ 
            console.log(data);
            $('tr.del').remove();
                
            data.deps.forEach( function(el){
            $('#departaments_list_tr').append('<tr id="tr'+el.id+'" class="del table-info edit_dep" data-id="'+el.id+'"><td><a href="#">'
            + el.name + '</a></td><td style="background-color: ' + el.color + '"></td></tr>')});
            $('.edit_dep').on('click', edit_dep);
        },
        error: function(data){
            $('#info').text(data);
            $('#info').dialog('open');
        }
    })
});

$('#dep_close_btn').click(function(){
    $('#departaments_list').dialog('close');
});

function edit_dep(data){

    id = $(this).data("id"); 

    $.ajax({
        type: "GET",
        url: '/departaments/getdep',                                
        data: {
            id : id
        },       
        success: function(data){ 
            console.log(data);
            
            $('#dep_id').val(id);
            $('#departament_name').val(data.name);
            $('#departament_color').val(data.color);
            $('#departament_color').css({backgroundColor: data.color });   

            $( "#creat_departament_form" ).dialog({ width: 500 });   
            $('#creat_departament_form').dialog('open');
            $('#departaments_list').dialog('close'); 
        },
        error: function(data){
            console.log(' Error!');
            console.log(data);  
            $('#info').text(data);
            $('#info').dialog('open');
        }                                           
    });   
};
    
/* Button Open Create Dep Modal */
$('#dep_creat_btn').click(function(){
    $('#creat_departament_form').dialog('open');
    $('#departaments_list').dialog('close');
});

    /* C R E A T  /  E D I T   D E P A R T A M E N T   F O R M   */

$('#creat_departament_form').dialog({
    autoOpen: false,
    width: 500,

    buttons: [
        {
            id: 'dep_create',   // DEP CREATE
            text: 'Создать',
            click: function(data){
                var name = $('#departament_name');
                var color = $('#departament_color');
                $.ajax({
                    type: "POST",
                    url: "/departaments/depadd",
                    data:{
                        name: name.val(),
                        color: color.val(),
                    }, 
                    response: 'text',
                    success: function(data){ 
                        // alert(data);
                        $('#info').text(data);
                        $('#info').dialog('open');
                        $('#creat_departament_form').dialog('close');
                         clearDepForm();  
                    },
                    error: function(data){
                        $('#info').text(data);
                        $('#info').dialog('open');
                    }
                });
            }
        },
        {
            id: 'dep_edit',     // DEP EDIT
            text: 'Изменить',
            click: function(data){
                var id = $('#dep_id').val();
                var name = $('#departament_name').val();
                var color = $('#departament_color').val();

                if(id == '') {
                    alert('Отдел не создан!');
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: "/departaments/edit",
                    data:{
                        id: id,
                        name: name,
                        color: color,
                    }, 
                    response: 'text',
                    success: function(data){ 
                        $('#info').text(data);
                        $('#info').dialog('open');
                        $('#creat_departament_form').dialog('close');
                    },
                    error: function(data){
                        $('#info').text(data);
                        $('#info').dialog('open');
                    }
                });
            }
        },
        {
            id: 'dep_del',     // DEP DELETE
            text: 'Удалить',
            click: function(data){

                id = $('#dep_id').val();

                if(id == '') {
                    alert('Отдел не создан!');
                    return false;
                }

                $.ajax({
                    type: "DELETE",
                    url: "/departaments/edit",
                    data:{
                        id: id
                    }, 
                    response: 'text',
                    success: function(data){ 
                        console.log(data);

                        $('#creat_departament_form').dialog('close');

                        if(data.depens){
                            console.log("dep has depencies");
                            $('#dialog_dep_delete').dialog('open');
                        } else {
                            console.log(data);
                            $('#info').text(data);
                            $('#info').dialog('open');
                        }                        
                    },
                    error: function(data){
                        $('#info').text(data);
                        $('#info').dialog('open');
                    }
                });
            }
        },
        {
            id: 'close_deps',
            text: 'Отмена',
            click: function(){
                clearDepForm();
                $('#creat_departament_form').dialog('close');
            }
        }
    ],
    show: {
        effect: "drop",
        duration: 500
    },

    hide: {
        effect: "drop",
        duration: 500
    }

});

/* LIST Departaments */
$('#departaments_list').dialog({
    autoOpen: false,
    width: 500,
    show: {
        effect: "drop",
        duration: 500
    },

    hide: {
        effect: "drop",
        duration: 500
    }
})

//  D E L E T E   D E P A R T A M E N T    W I T H  D E P E N C I E S  ///////////

$('#dialog_dep_delete').dialog({
    autoOpen: false,
    buttons: [ 
        {   
            id: 'cancel_del_dep',
            text: 'Отмена',
            click: function() { 
                $('#dialog_dep_delete').dialog('close');
            }
        },
        {
            id: 'del_dep_depenc',
            text: 'Удалить',
            click: function() { 

                id = $('#dep_id').val();

                $.ajaxSetup({
                    headers: {
                        'X-XSRF-TOKEN': getCookie("XSRF-TOKEN") ,
                    }
                });

                $.ajax({                        
                    type: "POST",
                    url: "/departaments/delDepDepenc",
                    data: {
                        id: id
                    },
                    success: function(data){
                        console.log(data); //for test
                        $('#dialog_dep_delete').dialog('close');
                        $('#info').text(data);
                        $('#info').dialog('open');
                        $('#calendar').fullCalendar( 'refetchEvents');                            
                    },
                    error: function(responce){
                        console.log('Error');
                        $('#dialog_dep_delete').dialog('close');
                        $('#info').text(data);
                        $('#info').dialog('open');
                    }
                });
            }
        }
    ],
    show: {
        effect: "drop",
        duration: 500
    },
    hide: {
        effect: "drop",
        duration: 500
    }
});

function clearDepForm(){
    $('#departament_name').val('');
    $('#departament_color').val('');
    $('#departament_color').removeAttr("style");
    $('#dep_id').val('');
    // console.log("dep_id" + $('#dep_id').val());
      
}
    