<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminPanel</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
{{--    <link href="{{asset('assets/css/style0.css')}}" rel="stylesheet">--}}
    <style>
        .mrgm { margin: 10px;}
    </style>

{{--    <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.0.min.js')}}"></script>--}}
    {{--calendar--}}
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/tcal.css')}}" />
    <script type="text/javascript" src="{{asset('assets/js/tcal.js')}}"></script>
    {{--end calendar--}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>
<body>
{{-- Main Menu - HEADER--}}
<header id="header wrapper">

    <h2 class="text-center"> {!! link_to (route('adm'), 'ПАНЕЛЬ АДМИНИСТРАТОРА' ) !!}</h2>

    <h3 class="text-center">{{ $title  }}</h3>

    <div class="d-flex justify-content-center">

        <button  type="button" class="btn btn-default bord mrgm">
            <span class="white">
                {!! link_to (route('events'), 'Go to Events List' ) !!}
            </span>
        </button>

        <button  type="button" class="btn btn-default bord mrgm">
            <span class="white">
                {!! link_to (route('categories'), 'Go to Categories List' ) !!}
            </span>
        </button>

        <button  type="button" class="btn btn-default bord mrgm">
            <span class="white">
                {!! link_to (route('users'), 'Go to Users List' ) !!}
            </span>
        </button>

        <button  type="button" class="btn btn-default bord mrgm">
            <span class="white">
                {!! link_to (route('departaments'), 'Go to Departaments List' ) !!}
            </span>
        </button>

    </div>

    {{--<div class="container">--}}
        {{--<div class="d-flex justify-content-center">--}}

            {{--<button  type="button" class="mrgm bg-orange white btn btn-default bord ">--}}
                {{--<span class="white">--}}
                    {{--{!! link_to (route('events'), 'Events' ) !!}--}}
                {{--</span>--}}
            {{--</button>--}}


        {{--</div>--}}
    {{--</div>--}}


    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if (count($errors) > 0)

        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>

        <div>
            <button  type="button" class="btn btn-warning bord mrgm">
            <span class="white">
{{--                {!! link_to (route('relatedEvents'), 'Show related EVENTS List' ) !!}--}}
                {!! link_to (route('home'), 'Show related EVENTS List user ' ) !!}
            </span>
            </button>

            <button  type="button" class="btn btn-warning bord mrgm">
            <span class="white">

{{--         {!! Html::link(route('relatedEvents/'{{$user_id}}, [$user->id]), $user->name,['alt'=>$user->name]) !!}--}}

            </span>
            </button>larave


            <button  type="button" class="btn btn-danger bord mrgm">
            <span class="white">
{{--                {!! link_to (route('deleteUserAndEvents'), 'Delete User with related Events List' ) !!}--}}
                {!! link_to (route('home'), 'Delete User with related Events List' ) !!}
            </span>
            </button>
        </div>



    @endif


    @yield('eventsList')
    @yield('eventAddContent')
    @yield('eventEditContent')

    @yield('usersList')
    @yield('userAddContent')
    @yield('userEditContent')

    @yield('categoriesList')
    @yield('categoryAddContent')
    @yield('categoryEditContent')

    @yield('departamentsList')
    @yield('departamentAddContent')
    @yield('departamentEditContent')


</header>

<br>
<br>
<hr class="my-4">
<br>

<div class="d-flex justify-content-center">
    <button  type="button" class="btn btn-default bord mrgm">
        <span class="white">
            {!! link_to (route('main'), 'Back to MAIN PAGE' ) !!}
        </span>
    </button>

    <button  type="button" class="btn btn-default bord mrgm">
        <span class="white">
            {!! link_to (route('adm'), 'Back to Admin Panel' ) !!}
        </span>
    </button>
</div>

<div class="d-flex justify-content-center">
    <button  type="button" class="btn btn-default bord mrgm">
        <span class="white">
            {!! link_to (route('home'), 'Back to Enter PAGE' ) !!}
        </span>
    </button>
</div>

<div class="d-flex justify-content-center">
    <button  type="button" class="btn btn-default bord mrgm">
        <a href="{{ route('logout') }}" onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
            Logout
        </a>
    </button>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST"style="display: none;">
    {{ csrf_field() }}
</form>



<br>
<br>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{url('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
</body>
