<h2 class="text-center">ADD NEW USER</h2>

<div class="wrapper container-fluid">

    {!! Form::open(['url'=>route('userAdd'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}

    {{--NAME--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
            </div>
            <div class="col-sm-8">
                {!! Form::text('name', old('name'), ['class' => 'form-control',
                                        'placeholder'=>'Input User name']) !!}
            </div>
        </div>
    </div>


    {{--email--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
            </div>
            <div class="col-sm-8">
                {!! Form::text('email', old('email'), ['class' => 'form-control',
                                        'placeholder'=>'Input User email']) !!}
            </div>
        </div>
    </div>


    {{--Role   ID--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('role_id', 'Role_id', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">

                <select class="form-control" id="roleidSelect" name="role_id">

                    @foreach( $roles as $role)
                        <option value="{{ $role->id }}"> {{ $role->name }} </option>
                    @endforeach

                </select>

            </div>
        </div>
    </div>


    {{--Departament--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('departament_id', 'Departament', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <select class="form-control" id="departamentSelect" name="departament_id">

                    @foreach( $departaments as $departament)
                        <option value="{{ $departament->id }}"> {{ $departament->name }} </option>
                    @endforeach

                </select>

            </div>
        </div>
    </div>


    {{--CATEGORY--}}
    {{--<div class="form-group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('us_category_id', 'User Category', ['class' => 'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--<div class="form-group">--}}
                    {{--<label for="exampleFormControlSelect1">Category select</label>--}}
                    {{--<select class="form-control" id="categorySelect" name="us_category_id">--}}

                        {{--@foreach( $uscategory as $uscat)--}}
                            {{--<option value="{{ $uscat->id }}"> {{ $uscat->name }} </option>--}}
                        {{--@endforeach--}}

                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--SIMPLE CHOISE CATEGORY--}}
    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-1"></div>--}}
    {{--{!! Form::label('event_category_id', 'Category', ['class' => 'col-sm-1 control-label']) !!}--}}
    {{--<div class="col-sm-8">--}}
    {{--{!! Form::text('event_category_id', old('event_category_id'), ['class' => 'form-control',--}}
    {{--'placeholder'=>'Input category_id']) !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--CATEGORY--}}
    {{--<div class="form-group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('event_category_id', 'Category', ['class' => 'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--<div class="form-group">--}}
                    {{--<label for="exampleFormControlSelect1">Category select</label>--}}
                    {{--<select class="form-control" id="categorySelect" name="category">--}}

                        {{--@foreach( $category as $cat)--}}
                            {{--<option value="{{ $cat->name }}"> {{ $cat->name }} </option>--}}
                        {{--@endforeach--}}

                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}








    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-1"></div>--}}
    {{--{!! Form::label('author_id', 'Author', ['class' => 'col-sm-1 control-label']) !!}--}}
    {{--<div class="col-sm-8">--}}
    {{--{!! Form::text('author_id', old('author_id'), ['class' => 'form-control',--}}
    {{--'placeholder'=>'Введите author_id']) !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}



    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-1"></div>--}}
    {{--{!! Form::label('images', 'Изображение', ['class' => 'col-sm-1 control-label']) !!}--}}
    {{--<div class="col-sm-6">--}}
    {{--{!! Form::file('images',['class'=>'filestyle','data-buttonText'=>'Выберите файл']) !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--password--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                {!! Form::label('password', 'password', ['class' => 'control-label']) !!}
            </div>
            <div class="col-sm-8">
                {!! Form::text('password', old('password'), ['class' => 'form-control',
                                        'placeholder'=>'Input password']) !!}
            </div>
        </div>
    </div>


    {{--SAVE BUTTON--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-offset-1 col-sm-8">
                {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

</div>


<div class="container-fluid up-cont dark-gr" style="height: 20px"></div>
