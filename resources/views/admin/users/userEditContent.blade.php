<hr class="my-4">
<br>
<h3 class="text-center">{{ $title }}</h3>
<h4></h4>
<br>
<hr class="my-4">

<div class="wrapper container-fluid">
    {!! Form::open(['url' => route('userEdit', array('category'=>$data['id'])), 'class'=>'form-horizontal', 'method'=>'POST', 'enctype'=>'multipart/form-data' ])!!}

    {{--NAME--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::hidden('id', $data['id']) !!}
            {!! Form::label('name', 'Name:', ['class'=>'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::text('name', $data['name'], ['class' => 'form-control', 'placeholder'=>'Input name!']) !!}

            </div>
        </div>
    </div>
    <br>

    {{--old Role--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('role_id', 'Old Role', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">

                {!! Form::text('role_id', $old_role, ['class' => 'form-control', 'placeholder'=>'Input name!']) !!}

            </div>

        </div>
    </div>

    {{--NEW ROLE--}}
    {{--<div class="form_group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('role_id', 'Choose New Role', ['class' => 'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--<select class="form-control" id="roleSelect" name="role">--}}
                    {{--<option value="user">User</option>--}}
                    {{--<option value="moderator">Moderator</option>--}}
                    {{--<option value="administrator">Administrator</option>--}}

                {{--</select>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <br>

    {{--Role   ID--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('role_id', 'Choose New Role', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">

                <select class="form-control" id="roleidSelect" name="role_id">

                    @foreach( $roles as $role)
                        <option value="{{ $role->id }}"> {{ $role->name }} </option>
                    @endforeach

                </select>

            </div>
        </div>
    </div>




    {{--old Department--}}
    {{--TODO--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('department_id', 'Old Department', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">

                {!! Form::text('department_id', $old_departament, ['class' => 'form-control', 'placeholder'=>'Input name!']) !!}

            </div>

        </div>
    </div>

    {{--Departament--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('departament_id', 'Departament', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <select class="form-control" id="departamentSelect" name="departament_id">

                    @foreach( $departaments as $departament)
                        <option value="{{ $departament->id }}"> {{ $departament->name }} </option>
                    @endforeach

                </select>

            </div>
        </div>
    </div>

    {{--us_cat--}}
    {{--<div class="form_group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::hidden('id', $data['us_category_id']) !!}--}}
            {{--{!! Form::label('us_category_id', 'User Category:', ['class'=>'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--{!! Form::text('us_category_id', $data['us_category_id'], ['class' => 'form-control', 'placeholder'=>'Input category!']) !!}--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<br>--}}


    {{--SUBMIT BUTTON--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-offset-2 col-sm-8">
                {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>
    <br>

    {!! Form::close() !!}

</div>

