<hr class="my-4">

<h2 class="text-center">EVENTS LIST</h2>


<div class="container-fluid">

    @if($events)

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Start Date</th>
                <th>Title</th>
                <th>Creator</th>
                <th>Perfomer</th>
                <th>Departament</th>
                <th>Data Created</th>
                <th>Delete</th>
            </tr>
            </thead>

            @foreach( $events as $event)
                <tr>
                    <td>{{ $event->start }}</td>
                    <td>{!! Html::link(route('eventEdit', ['event'=>$event->id]), $event->title,['alt'=>$event->title]) !!}</td>
                    <td>{{ $event->creator->name }}</td>
                    <td>{{ $event->user->name }}</td>
                    <td>{{ $event->departament->name }}</td>
                    <td>{{ $event->created_at }}</td>
                    <td>
                        {!!  Form::open(['url' => route('eventEdit', ['event'=>$event->id]), 'class'=>'form-horizontal', 'method'=>'POST'])    !!}
                        {{ method_field('DELETE') }}
                        {!! Form::button('Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </table>

    @endif

    <button  type="button" class="bg-orange white btn btn-default bord "><span class="white">{!! Html::link(route('eventAdd'), 'Create a New Event') !!}</span></button>




</div>

