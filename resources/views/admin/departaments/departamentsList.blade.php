<hr class="my-4">

<h2 class="text-center">DEPARTAMENTS LIST</h2>


<div class="container-fluid">

    @if($departaments)

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th># Number</th>
                <th>name</th>
                <th>Data Created</th>
                <th>Delete</th>
            </tr>
            </thead>

            @foreach( $departaments as $departament)
                <tr>
                    <td>{{$departament->id}}</td>
                    <td>{!! Html::link(route('departamentEdit', ['departament'=>$departament->id]), $departament->name,['alt'=>$departament->name]) !!}</td>
                    <td>{{ $departament->created_at }}</td>
                    <td>
                        {!!  Form::open(['url' => route('departamentEdit', ['departament'=>$departament->id]), 'class'=>'form-horizontal', 'method'=>'POST'])    !!}
                        {{ method_field('DELETE') }}
                        {!! Form::button('Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </table>

    @endif

    <button  type="button" class="bg-orange white btn btn-default bord "><span class="white">{!! Html::link(route('departamentAdd'), 'Create a New Departament') !!}</span></button>




</div>
