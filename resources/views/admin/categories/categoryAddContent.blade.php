<h2 class="text-center">ADD NEW CATEGORY</h2>

<div class="wrapper container-fluid">

    {!! Form::open(['url'=>route('categoryAdd'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}

    {{--NAME--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                {!! Form::label('name', 'Category Name', ['class' => 'control-label']) !!}
            </div>
            <div class="col-sm-8">
                {!! Form::text('name', old('name'), ['class' => 'form-control',
                                        'placeholder'=>'Input name category']) !!}
            </div>
        </div>
    </div>

    {{--DESCRIPTION--}}
    {{--<div class="form-group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('description', 'Description', ['class' => 'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--{!! Form::textarea('description', old('description'), ['class' => 'form-control',--}}
                                        {{--'placeholder'=>'Input Description']) !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--SIMPLE CHOISE CATEGORY--}}
    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-1"></div>--}}
    {{--{!! Form::label('event_category_id', 'Category', ['class' => 'col-sm-1 control-label']) !!}--}}
    {{--<div class="col-sm-8">--}}
    {{--{!! Form::text('event_category_id', old('event_category_id'), ['class' => 'form-control',--}}
    {{--'placeholder'=>'Input category_id']) !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--CATEGORY--}}
    {{--<div class="form-group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('event_category_id', 'Category', ['class' => 'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--<div class="form-group">--}}
                    {{--<label for="exampleFormControlSelect1">Category select</label>--}}
                    {{--<select class="form-control" id="categorySelect" name="category">--}}

                        {{--@foreach( $category as $cat)--}}
                            {{--<option value="{{ $cat->name }}"> {{ $cat->name }} </option>--}}
                        {{--@endforeach--}}

                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}








    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-1"></div>--}}
    {{--{!! Form::label('author_id', 'Author', ['class' => 'col-sm-1 control-label']) !!}--}}
    {{--<div class="col-sm-8">--}}
    {{--{!! Form::text('author_id', old('author_id'), ['class' => 'form-control',--}}
    {{--'placeholder'=>'Введите author_id']) !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}



    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-1"></div>--}}
    {{--{!! Form::label('images', 'Изображение', ['class' => 'col-sm-1 control-label']) !!}--}}
    {{--<div class="col-sm-6">--}}
    {{--{!! Form::file('images',['class'=>'filestyle','data-buttonText'=>'Выберите файл']) !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    {{--SAVE BUTTON--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-offset-1 col-sm-8">
                {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

</div>


<div class="container-fluid up-cont dark-gr" style="height: 20px"></div>
