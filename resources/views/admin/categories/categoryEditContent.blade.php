<hr class="my-4">
<br>
<h3 class="text-center">{{ $title }}</h3>
<h4></h4>
<br>
<hr class="my-4">

<div class="wrapper container-fluid">
    {!! Form::open(['url' => route('categoryEdit', array('category'=>$data['id'])), 'class'=>'form-horizontal', 'method'=>'POST', 'enctype'=>'multipart/form-data' ])!!}

    {{--NAME--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::hidden('id', $data['id']) !!}
            {!! Form::label('name', 'Name:', ['class'=>'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::text('name', $data['name'], ['class' => 'form-control', 'placeholder'=>'Input name!']) !!}

            </div>
        </div>
    </div>
    <br>

    {{--SUBMIT BUTTON--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-offset-2 col-sm-8">
                {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>
    <br>

    {!! Form::close() !!}

</div>

