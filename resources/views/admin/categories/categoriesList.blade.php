<hr class="my-4">

<h2 class="text-center">CATEGORIES LIST</h2>


<div class="container-fluid">

    @if($categories)

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th># Number</th>
                <th>name</th>
                <th>Data Created</th>
                <th>Delete</th>
            </tr>
            </thead>

            @foreach( $categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{!! Html::link(route('categoryEdit', ['category'=>$category->id]), $category->name,['alt'=>$category->name]) !!}</td>
                    <td>{{ $category->created_at }}</td>
                    <td>
                        {!!  Form::open(['url' => route('categoryEdit', ['category'=>$category->id]), 'class'=>'form-horizontal', 'method'=>'POST'])    !!}
                        {{ method_field('DELETE') }}
                        {!! Form::button('Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </table>

    @endif

    <button  type="button" class="bg-orange white btn btn-default bord "><span class="white">{!! Html::link(route('categoryAdd'), 'Create a New Category') !!}</span></button>




</div>
