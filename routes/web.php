<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\Auth\LoginController;

Route::group(['middleware'=>'web'], function () {

    Route::get('/users/status', ['uses' => "Calendar\UserController@userstatus", 'as' => 'status']);

});

Route::group(['middleware'=>'auth'], function () {

    Route::get('/', ['uses' => 'MainController@execute', 'as' => 'main']);

    Route::get('/events', ['uses' => 'MainController@showevents', 'as' => 'showevents']);

    Route::get('/calendar', function () {

         return url('http://laravelcalendar.example/calendar/index.html');
    })->name('calendar');

    Route::get('/getCsv', function () {

        return ('http:///home/dok/projects/websites/laravelcalendar/public/data.csv');
    })->name('getCsv');

});


Route::group(['prefix' => 'users', 'middleware' => 'auth'], function () {

    Route::match(['get', 'post'], '/add', ['uses' => 'Calendar\UserController@adduser', 'as' => 'calendarUserAdd']);

    Route::get('/getuser', ['uses' => 'Calendar\UserController@getuser', 'as' => 'getuser']);

    Route::get('/usersdata', ['uses' => 'Calendar\UserController@usersdata', 'as' => 'usersdata']);

    Route::get('/remind', ['uses' => 'Calendar\UserController@remind', 'as' => 'remind']);

    Route::get('/usersalldata', ['uses' => 'Calendar\UserController@usersalldata', 'as' => 'usersalldata']);

    Route::post('/delUserChange', ['uses' => 'Calendar\UserController@delUserChange', 'as' => 'delUserChange']);

    Route::match(['get', 'post', 'delete'],'/userdelete', ['uses' => 'Calendar\UserController@userdelete', 'as' => 'userdelete']);

    Route::match(['get', 'post', 'delete'],'/edit', ['uses' => 'Calendar\UserController@edit', 'as' => 'useredit']);

});

Route::group(['prefix' => 'events', 'middleware' => 'auth'], function () {

    Route::get('/getRemind', ['uses' => 'Calendar\EventController@getRemind', 'as' => 'getRemind']);

    Route::get('/listEventsMonth', ['uses' => 'Calendar\EventController@listEventsMonth', 'as' => 'listEventsMonth']);

    Route::get('/listEventsDay', ['uses' => 'Calendar\EventController@listEventsDay', 'as' => 'listEventsDay']);

    Route::get('/getCsvList', ['uses' => 'Calendar\EventController@getCsvList', 'as' => 'getCsvList']);
    Route::get('/foo', ['uses' => 'Calendar\EventController@foo', 'as' => 'foo']);

    Route::get('/getEvent', ['uses' => 'Calendar\EventController@getEvent', 'as' => 'getEvent']);

    Route::get('/getAllEvents', ['uses' => 'Calendar\EventController@getAllEvents', 'as' => 'getAllEvents']);

    Route::match(['get', 'post'], '/add', ['uses' => 'Calendar\EventController@execute', 'as' => 'calendarEventAdd']);

    Route::match(['get', 'post', 'delete'], '/edit', ['uses' => 'Calendar\EventController@edit', 'as' => 'eventedit']);

});

Route::group(['prefix' => 'departaments', 'middleware' => 'auth'], function () {

    Route::get('/depsdata', ['uses' => 'Calendar\DepartamentController@depsdata', 'as' => 'depsdata']);

    Route::get('/getdep', ['uses' => 'Calendar\DepartamentController@getdep', 'as' => 'getdep']);

    Route::match(['get', 'post', 'delete'],'/edit', ['uses' => 'Calendar\DepartamentController@edit', 'as' => 'depedit']);

    Route::match(['get', 'post'],'/depadd', ['uses' => 'Calendar\DepartamentController@depadd', 'as' => 'depadd']);

    Route::match(['get', 'post', 'delete'],'/delDepDepenc', ['uses' => 'Calendar\DepartamentController@delDepDepenc', 'as' => 'delDepDepenc']);

});

Route::group(['prefix' => 'categories', 'middleware' => 'auth'], function () {

});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    /*Admin   for main admin page */
    Route::get('/', function () {
        if (view()->exists('admin.index')) {
            $data = ['title' => 'Admin Panel'];
            return view('admin.index', $data);
        }
    })->name('adm');

    //admin/events
    Route::group(['prefix' => 'events'], function () {
        //admin/events List
        Route::get('/', ['uses' => 'Admin\AdminEventController@execute', 'as' => 'events']);
        // calendar
        Route::get('/gotevent', ['uses' => 'Admin\AdminEventController@gotevent', 'as' => 'gotevent']);

        Route::get('/relatedEvents/{id}', ['uses' => 'Admin\AdminEventController@relatedEvents', 'as' => 'relatedEvents']);
        //admin/events/add
        Route::match(['get', 'post'], '/add', ['uses' => 'Admin\AdminEventAddController@execute', 'as' => 'eventAdd']);
        //admin/edit/2
        Route::match(['get', 'post', 'delete'], '/edit/{event}', ['uses' => 'Admin\AdminEventEditController@execute', 'as' => 'eventEdit']);
    });
    //admin/categories
    Route::group(['prefix' => 'categories'], function () {
        //admin/categories List
        Route::get('/', ['uses' => 'Admin\AdminCategoryController@execute', 'as' => 'categories']);
        //admin/category/add
        Route::match(['get', 'post'], '/add', ['uses' => 'Admin\AdminCategoryController@addcategory', 'as' => 'categoryAdd']);
        //admin/category/edit/2
        Route::match(['get', 'post', 'delete'], '/edit/{category}', ['uses' => 'Admin\AdminCategoryController@editcategory', 'as' => 'categoryEdit']);
    });

    //admin/departaments
    Route::group(['prefix' => 'departaments'], function () {
        //admin/departaments List
        Route::get('/', ['uses' => 'Admin\AdminDepartamentController@execute', 'as' => 'departaments']);
        //admin/Department/add
        Route::match(['get', 'post'], '/add', ['uses' => 'Admin\AdminDepartamentController@adddepartament', 'as' => 'departamentAdd']);
        //admin/Department/edit/2
        Route::match(['get', 'post', 'delete'], '/edit/{departament}', ['uses' => 'Admin\AdminDepartamentController@editdepartament', 'as' => 'departamentEdit']);
    });

    //admin/users
    Route::group(['prefix' => 'users'], function () {
        //admin/users List
        Route::get('/', ['uses' => 'Admin\AdminUserController@execute', 'as' => 'users']);
        //admin/user/add
        Route::match(['get', 'post'], '/add', ['uses' => 'Admin\AdminUserController@adduser', 'as' => 'userAdd']);
        //admin/user/edit/2
        Route::match(['get', 'post', 'delete'], '/edit/{user}', ['uses' => 'Admin\AdminUserController@edituser', 'as' => 'userEdit']);
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
